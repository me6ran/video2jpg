
import argparse
import cv2
import os

ap = argparse.ArgumentParser()
ap.add_argument("-n","--number",required=False, type=int, default=0, help="Start generting number from n for naming image files")
ap.add_argument("-r","--rate",required=True, type=int, help="ratio between taking picture and frames\ne.g. if -r 2, it will take 1 picture each 2 frames")
ap.add_argument("-f","--file",required=True,type=str,help="path to Video file (including file name)")
args = vars(ap.parse_args())


baseAddress=(os.getcwd())
dataFolder=os.path.join(baseAddress,"data")
if not (os.path.isdir(dataFolder)):
    os.mkdir(baseAddress+"/data")

number=args["number"]
count=0
cap = cv2.VideoCapture(args["file"])

while (cap.isOpened()):
    ret,frame=cap.read()
    #print(ret)
    count+=1
    if(ret):
        #frame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
        if(count==args["rate"]):
            filename="data/"+"%04d"%number+".jpg"
            cv2.imwrite(filename,frame)
            number+=1
            count=0
        
        cv2.imshow("Image",frame)
        if(cv2.waitKey(1)== ord("q")):
            break
    else:
        break
print("Cleaning up...")
cv2.destroyAllWindows()