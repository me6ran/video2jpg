import cv2
import os


generatedFileName=1
PICQUANTITY=7
fileList=[]

if not (os.path.isdir("./Pictures")):
    os.mkdir("./Pictures")
    print("Pictures folder has been made.")

if not (os.path.isfile('namingNumber')):
    fp=open('namingNumber',"w")
    fp.write('1')
    fp.close()
    print("File number has been initialized.")
else:
    fp=open('namingNumber',"r")
    try:
        generatedFileName=int(fp.readline())
    except:
        pass
    print("File numbnet is at %5d" %(generatedFileName))

for name in os.listdir("."):
    if(name.endswith("mp4")):
        fileList.append(name)



def countingFrame(fileName):
    count=0
    cap=cv2.VideoCapture(fileName)
    while True:
        ret, frame= cap.read()
        if(ret):
            count+=1
        else:
            break
    print("File: %s has %d frames" % (fileName,count))
    return count



def savingFrames(fileName, Step):
    count=0
    global generatedFileName
    cap=cv2.VideoCapture(fileName)
    while True:
        ret, frame = cap.read()
        count+=1
        #print(count)
        if(ret):
            if((count==Step)):
                try:
                    outputFileName="Pictures/"+"%05d"%generatedFileName+".jpg"
                    #print("creating pictures...")
                    frame=cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
                    cv2.imwrite(outputFileName, frame)
                    count=0
                    generatedFileName+=1
                except:
                    continue
        else:
            break



if __name__ == '__main__':

    for file in fileList:
        FramesCounted=countingFrame(file)
        jump=int(FramesCounted/PICQUANTITY)
        if(jump<=1):
            continue
        savingFrames(file,jump)
        fp=open('namingNumber',"w")
        fp.write(str(generatedFileName))
        fp.close()
        print("File number has been updated.")

    print("%d Pictures has been extracted." % (generatedFileName-1))