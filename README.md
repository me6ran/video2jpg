the script is for collecting pictures out of video files by giving the ratio of frame/pic.

you need to have opencv installed on your system and it is tested on GNU/Linux Ubuntu.




usage: video2Jpg.py [-h] [-n NUMBER] -r RATE -f FILE

optional arguments:
  -h, --help            show this help message and exit
  -n NUMBER, --number NUMBER
                        Start generting number from n for naming image files
  -r RATE, --rate RATE  ratio between taking picture and frames e.g. if -r 2,
                        it will take 1 picture each 2 frames
  -f FILE, --file FILE  path to Video file (including file name)


I had to copy from other's git repo and modify some codes for putting lables and annotating for photos.
