import os
import matplotlib.pyplot as plt
import cv2
from matplotlib.widgets import RectangleSelector
from annotator import write_xml


img = None
tl_list = []
br_list = []
object_list = []

image_folder = '../data'
savedir = 'annotations'
obj = 'person'

imgName=''
trackfile="trackFile"
fileNumber=None
EXIT=False

errorFile="errors"

if not (os.path.isdir(savedir)):
    os.mkdir(savedir)
    

if not (os.path.isfile(errorFile)):
    fd=open(errorFile,"w")
    fd.close()

if(os.path.isfile(trackfile)):
    fd= open(trackfile,"r")
    try:
        fileNumber=fd.readline()
        fd.close()
    except:
        fd.close()
        pass
else:
    fd=open(trackfile,"w")
    fd.close()

def updateFile(fileName,content):
    if(fileName==errorFile):
        fd=open(fileName,'a')
        fd.write(content+"\n")
        fd.close()
    else:
        fd=open(fileName,'w')
        fd.write(content)
        fd.close()


def line_select_callback(clk, rls):
    global tl_list
    global br_list
    global object_list
    tl_list.append((int(clk.xdata), int(clk.ydata)))
    br_list.append((int(rls.xdata), int(rls.ydata)))
    object_list.append(obj)


def onkeypress(event):
    global object_list
    global tl_list
    global br_list
    global imgName,EXIT
    global errorFile
    global trackfile
    global img
    print(tl_list,br_list,obj)
    if(event.key=="r"):
        print("coordinates has been removed.")
        tl_list = []
        br_list = []
        object_list = []
        plt.show()
    elif(event.key=="q"):
        if not ((object_list) and (br_list) and (tl_list)):
            #msg="No coordinates or object name set for file: %s" % (image_file.name)
            #print(image_file.name)
            updateFile(errorFile,"No coordinates or object name has been set for file: %s" % (image_file.name))
            #print("no data annotated!",imgName)
            #updateFile(errorFile,number)
            #plt.close()
        
        else:
            write_xml(image_folder, img, object_list, tl_list, br_list, savedir)
            tl_list = []
            br_list = []
            object_list = []
            img=None
            updateFile(trackfile,imgName)
            plt.close()

    elif(event.key=="d"):
        plt.close()
        tl_list = []
        br_list = []
        os.remove(image_file.path)
        print("File: %s has been deleted." %(imgName))
    elif(event.key=="x"):
        EXIT=True
        plt.close()
        tl_list = []
        br_list = []


def toggle_selector(event):
    toggle_selector.RS.set_active(True)


if __name__ == '__main__':

    for n, image_file in enumerate(os.scandir(image_folder)):
        #print(n,image_file.name)
        if(EXIT):
            break
        if(fileNumber):
            if( image_file.name != fileNumber):
                continue
            else:
                fileNumber=None
                continue
        img=image_file
        imgName= image_file.name   
        print("File: %s" % (imgName))
        fig, ax = plt.subplots(1)
        image = cv2.imread(image_file.path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        ax.imshow(image)

        toggle_selector.RS = RectangleSelector(
            ax, line_select_callback,
            drawtype='box', useblit=True,
            button=[1], minspanx=5, minspany=5,
            spancoords='pixels', interactive=True)

        bbox = plt.connect('key_press_event', toggle_selector)
        key = plt.connect('key_press_event', onkeypress)
        plt.show()

